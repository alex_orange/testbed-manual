"""This profile demonstrates how to add some extra *local* disk space on your
node. In general nodes have much more disk space then what you see with `df`
when you log in. That extra space is in unallocated partitions or additional
disk drives. An *ephemeral blockstore* is how you ask for some of that space to
be allocated and mounted as a **temporary** filesystem (temporary means it will
be lost when you terminate your experiment).

Instructions:
Log into your node, your **temporary** file system in mounted at `/mydata`.
"""

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as rspec
# Import the emulab extensions library.
import geni.rspec.emulab

# Create a Request object to start building the RSpec.
request = portal.context.makeRequestRSpec()

# Allocate a node and ask for a 30GB file system mounted at /mydata
node = request.RawPC("node")
bs = node.Blockstore("bs", "/mydata")
bs.size = "30GB"

# Print the RSpec to the enclosing page.
portal.context.printRequestRSpec()
