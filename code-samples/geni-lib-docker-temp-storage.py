"""An example of a Docker container that mounts a remote blockstore."""

import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.igext as ig

request = portal.context.makeRequestRSpec()
node = request.DockerContainer("node")
bs = node.Blockstore("temp-bs","/mnt/tmp")
bs.size = "8GB"
bs.placement "any"

portal.context.printRequestRSpec()
